module.exports = [
  {
    name: "Produto I",
    price: 23.54,
    image: "https://picsum.photos/200",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit",
  },
  {
    name: "Produto II",
    price: 10.25,
    image: "https://picsum.photos/200",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit",
  },
  {
    name: "Produto X",
    price: 12.3,
    image: "https://picsum.photos/200",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit",
  },
  {
    name: "Produto A",
    price: 5.25,
    image: "https://picsum.photos/200",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit",
  },
  {
    name: "Produto F",
    price: 15.69,
    image: "https://picsum.photos/200",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit",
  },
  {
    name: "Produto M",
    price: 149.99,
    image: "https://picsum.photos/200",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit",
  }
];

// module.exports = [{
//     "name": "Berries",
//     "price": 23.54,
//     "image": "/assets/images/berries.jpeg",
//     "description": "The bestest fruit known to man. Sweet yet sour but beautiful"
//   },
//   {
//     "name": "Orange",
//     "price": 10.33,
//     "image": "/assets/images/oranges.jpeg",
//     "description": "Succulent and watery, you'll never run out of water"
//   },
//   {
//     "name": "Lemons",
//     "price": 12.13,
//     "image": "/assets/images/lemons.jpeg",
//     "description": "Sour but important for revitalization"
//   },
//   {
//     "name": "Bananas",
//     "price": 10.33,
//     "image": "/assets/images/banana.jpeg",
//     "description": "An every day fruit, can be served with every dish"
//   },
//   {
//     "name": "Apples",
//     "price": 10.33,
//     "image": "/assets/images/apple-item.png",
//     "description": "Sliced and served with your salad. Served as snacks midway through the day"
//   },
//   {
//     "name": "Sharifa",
//     "price": 10.33,
//     "image": "/assets/images/unknown.jpeg",
//     "description": "A great fruit, also known as custard apple"
//   }
// ]
