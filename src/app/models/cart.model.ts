import { Product } from "./product.model";

export class Cart {
  public items: Product[] = [];
  public total: number = 0;
}
