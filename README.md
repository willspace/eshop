[![Open Source Love svg2](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# eShop

O eShop é um e-commerce desenvolvido com angular, redux e node.js. A primeira versão traz apenas uma
simples demonstração de como o padrão redux pode atuar gerenciando os estados de uma aplicação, tornando
o gerenciamento mais fácil e trazendo uma estrutura mais organizada aos projetos.

Caso queira saber mais sobre esse maravilhoso padrão, eis um artigo interessante pra você: [React States vs Redux States: Quando e Por quê?](https://medium.com/reactbrasil/react-states-vs-redux-states-quando-e-porqu%C3%AA-6339436553f3)

## Tecnologias utilizadas

 Front-end

 * [Angular 9.1.0](https://github.com/angular/angular-cli)
 * [UI Kit Framework](https://getuikit.com/)
 * [Padrão Redux (NgRX/Store | Ngrx/Effects)](https://ngrx.io/guide/store)
 * [Lazy Loading](https://angular.io/guide/lazy-loading-ngmodules)
 * [Angular Routing](https://angular.io/start/start-routing)

 Back-end

 * [Node.js](https://nodejs.org/)
 * [Express Framework](https://expressjs.com/pt-br/)

 Recursos

 * [Lorem Picsum](https://picsum.photos/) - Gerador de imagens aleatórias
 * [Fontes do Google](https://fonts.google.com/) - (Dosis)
 * [Toastr](https://www.npmjs.com/package/ngx-toastr)
 * [Font Awesome](https://fontawesome.com/)

## Execução

 * Clone o projeto
 * Vá até a pasta clonada e instale as dependências com `npm install`
 * Em um terminal vá até a pasta 'server' dentro do projeto e execute o comando `node server`
 * Execute o comando `ng serve` e abra a aplicação no navegador em localhost:4200

---

<p align="center">Feito com ❤️ por <strong>Will :wave: </p>